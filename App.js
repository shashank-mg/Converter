import React from "react";
import ReactDOM from "react-dom";
import Calculator from "./Calculator";

function App() {
  return (
    <div className="App">
        <Calculator/>
    </div>
  );
}

export default App;
