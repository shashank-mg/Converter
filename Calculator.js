import React from 'react';
import ReactDOM from 'react-dom';
export default class Calculator extends React.Component
{
  constructor(props){
    super(props)
    this.state={scale:'c',temp:0}
  }
handleFahrenheit=(e)=>{
  this.setState({scale:'f',temp:e.target.value})
}
handleCelsius=(e)=>{
  this.setState({scale:'c',temp:e.target.value})
}
render()
{
  const temp=this.state.temp
  const scale=this.state.scale
  const celsius=scale==='f'?(temp-32)*5/9:temp;
  const fahrenheit=scale==='c'?(temp*9/5)+32:temp;
  return(
    <React.Fragment>
    <legend> Fahrenheit </legend>
    <input type='number' value={fahrenheit} id="Fahrenheit" onChange={this.handleFahrenheit}/>

    <legend> Celsius </legend>
    <input type='number' value={celsius} id="Celsius" onChange={this.handleCelsius}/>
    </React.Fragment>
  );
}
}
